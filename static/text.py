intro = "Throughout the Rocky Mountains, towering pine trees dominate the landscape. Lodgepole pines, *Pinus contorta*, " \
      "shown above, are common examples of Rocky Mountain conifers. The habitat of the four subspecies of lodgepole " \
      "pines extends from Alaska to southern California, and so these pine trees are ecologically critical. They " \
      "provide housing and food for other organisms. "

intro2 = "Two of these animals include Pine Squirrels and Crossbill birds."

intro3 = "Both the squirrels and crossbills rely on lodgepole pinecones as an important food source. However, because " \
         "lodgepole pines have such a wide distribution, there are a few populations of trees which are considered " \
         "\"islands,\" some of which do not have any squirrels. This can be seen in the distribution map. "

intro4 = "Researchers have longed noticed differences in pinecones across these populations. Sometimes they have an " \
         "open, larger structure, which makes them harder for squirrels to eat. But when there are no squirrels, " \
         "oftentimes the pinecones  become more closed up, and thus harder for the crossbills to eat."


askingq1 = "In the paper [Replicated population divergence caused by localized coevolution: a test of three hypotheses" \
            " in the red crossbill-lodgepole pine system](http://www.uwyo.edu/benkman/pdfs%20of%20papers/edelaar&benkman2006.pdf) " \
            "the researchers measured cone mass and crossbill beak depth " \
            "in island and mainland populations with and without squirrels to test whether the presence/" \
            "absence of squirrels affected cone mass and crossbill morphology. The researchers’ comparisons of these " \
            "populations supported the hypothesis that cone morphology is due to the presence or absence of squirrels, " \
            "not island status or crossbill type. " \
            "This led us to ask the question, __can we predict pinecone mass based on habitat features?__ "

askingq2 = "For our analysis, we contacted one of the primary authors, Pim Edelaar, for access " \
            "to the cone mass data, and used data collated " \
            "in Whitlock & Schluter's textbook [Analysis of biological data](http://whitlockschluter.zoology.ubc.ca/) " \
            "to see whether we can predict pinecone mass from population features."

understandd1 = "Let's take a look at the data from the original paper; this data is stored in a csv called `r merged.csv` " \
               "and contains features about each population as well as the average cone mass, length, and width. "

understandd2 = "These are a subset of the sampling sites the original paper collected data from. All of them are shown " \
               "on the following map, along with whether they were mainland populations or not."

understandd3 = "The data from Whitlock & Schluter is a series of measurements of different habitat types, along with" \
               " average conemasses from those populations. "

understandd4 = "Just by looking, you can see that there is a pretty big difference between the conemass in habitats with" \
               "and without squirrels, but it doesn't seem that there is as large a difference between the island " \
               "versus mainland populations. But is this actually true? Let's run an ANOVA to see if there is actually " \
               "a difference in the means of these groups.  "

understand5 = """

What are our statistical hypothesises? 
  **Null** : No difference in mean of cone size between habitats 
  **Alternate** : A difference in mean of cone size between habitats. 
  **Alpha** : 0.05. 

"""

understand6 = """


In the ANOVA performed above, the P value is very small (< 0.05) so we can reject the null, meaning at least one mean between the habitats is different, but which one?

We will run a comparison of two of the sample means, the island absent and island present. This is planned comparison, one we would expect would be different from the beginning. Two sample t-tests should not be used for unplanned comparisons, because the probabilty of Type 1 error compounds. 
"""

understand7 = """Two Sample t-test
```
data:  conemass by habitat
t = 8.1371, df = 9, p-value = 1.932e-05
alternative hypothesis: true difference in means is not equal to 0
95 percent confidence interval:
 2.036027 3.603973
sample estimates:
 mean in group island.absent mean in group island.present 
                        8.90                         6.08 
```                        
"""

understand8 = """
We've shown there is a significant difference between these two groups, because the p-value is much smaller than our alpha of 0.05. Thus, even before we build our model, we know where we should begin with our feature selection. Including squirrels is smart, because we know that squirrels are effecting the conemass size from the Whitlock & Schluter data.

Let's return to the data from the original paper, and try to build a linear model to predict conemass size! 

"""

featureselect1 = """
We are going to be using linear regression to predict conemass.

**Regression** : method to predict values of response from explanatory variables

In general the regression line for a population is of the form 
`Y=α+βX` where α (the intercept) and β (the slope) are population parameters.


Multiple linear regression is an expanded linear regression with multiple predictors or explanatory variables. 
y^=β0+β1x1+β2x2+⋯+βkxk

You can use the R^2 or adjusted R^2 as the metric for choosing your model. 

For our model we will be using forward selection: adding variables with largest improvement of R^2 and small P-values
Starting with the squirrels as a predictor is a good idea, because from the Whitlock & Schluter data we already
know it is likely to be an important predictor. 
"""

model1 = """
Let's build our first model. The commands in R to create such a linear model are
```r
mdl1<- lm(  mydata$cone.mass ~ mydata$squirrels. )
summary(mdl1)
```
"""

model1table = """
Coefficients:
"""
model1table2 = """
Signif. codes:  0 ‘\*\*\*’ 0.001 ‘\*\*’ 0.01 ‘\*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 1.008 on 9 degrees of freedom

Multiple R-squared:  0.5657,	**Adjusted R-squared:**  0.5174 

F-statistic: 11.72 on 1 and 9 DF,  p-value: 0.007584

"""

model2 = """
To build the second model use these commands:
```
md111 <- lm(cone.mass ~ squirrels.+ lat + long, data=mydata)
summary(md111)
```

"""

model2table2 = """
Signif. codes:  0 ‘\*\*\*’ 0.001 ‘\*\*’ 0.01 ‘\*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 1.139 on 7 degrees of freedom

Multiple R-squared:  0.5683,	**Adjusted R-squared:**  0.3834 

F-statistic: 3.072 on 3 and 7 DF,  p-value: 0.1001



Hmm, as you can see, using the location (latitude and longitude) did not improve the model.
The adjusted R-squared dropped using this model, so in our next model, we don't use the location data.
"""

model3 = """
For this model, let's see if the crossbills improve prediction. 
To build the third model use these commands:
```
md12 <- lm(cone.mass ~ squirrels. + bills., data=mydata)
summary(md12)
```
"""

model3table2 = """
Signif. codes:  0 ‘\*\*\*’ 0.001 ‘\*\*’ 0.01 ‘\*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 1.037 on 8 degrees of freedom

Multiple R-squared:  0.5911,	**Adjusted R-squared:**  0.4889 

F-statistic: 5.782 on 2 and 8 DF,  p-value: 0.02796



This model also did not improve our R-squared! It seems that for this sample set, 
the best predictor is always squirrel presence. Because the squirrels are a binary measurement 
(either they were there or they weren't) the final model (the first model) essentially represent two averages.
Sometimes stats can't save you ¯\\\_(ツ)\_/¯


[Find our code here](https://gitlab.com/megretson/critters-and-cones)
"""





